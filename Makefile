MODE ?= xen
NET	 ?= direct
HOST ?= localhost
DHCP  = true

FLAGS  ?=

.PHONY: configure build clean


configure:
	env NET=$(NET) DHCP=$(DHCP) mirage configure src/config.ml --$(MODE)

depend:
	cd src && make depend

build:
	cd src && make build

clean:
	cd src && make clean
	$(RM) log src/mir-store

run :
	./src/mir-unistore

disk:
	cd disk && fat create store.img 102400KiB
