open Mirage


let net =
  try match Sys.getenv "NET" with
    | "direct" -> `Direct
    | _        -> `Socket
  with Not_found -> `Direct

let dhcp =
  try match Sys.getenv "DHCP" with
    | "true" -> true
    | _   -> false
  with Not_found -> false

let stack console =
  match net, dhcp with
  | `Direct, true  -> direct_stackv4_with_dhcp console tap0
  | `Direct, false -> direct_stackv4_with_default_ipv4 console tap0
  | `Socket, _     -> socket_stackv4 console [Ipaddr.V4.any]

let server =
  http_server (conduit_direct (stack default_console))

let disk =
  fat (block_of_file "../disk/store.img")

let libraries = ["cow.syntax"; "cow"]
let packages  = ["cow"]

let main =
  foreign ~libraries ~packages "Unikernel.Main" (console @-> http @-> fs @-> job)

(* let tracing = mprof_trace ~size:1000000 () *)

let () =
  register "unistore" [
    main $ default_console $ server $ disk
  ]
