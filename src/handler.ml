open Lwt
open Cohttp
open Cow

module Make (Server: Cohttp_lwt.Server) (F: V1_LWT.FS) = struct

  type dir_content = {
    files : string list;
  } with json


  let send_reponse code msg =
    Server.respond_string ~status: (`Code code) ~body:(Printf.sprintf "%s\n" msg) ()


  (* path handlers *)
  let hello req body =
    Log.print_req req body >>= fun _ ->
    send_reponse 200 "Hello World"


  let list_files req body fs =
    F.listdir fs "/" >>= function
    | `Ok s ->
      let resp = {files = s} in
      let resp_json = Cow.Json.to_string (json_of_dir_content resp) in
      send_reponse 200 resp_json

    | `Error e -> Log.warn "error while listing contents\n" >>= fun _ ->
      send_reponse 500 "error occured while listing contents"


  let page_size   = Io_page.round_to_page_size 1
  let pages_needed size =
    (size + page_size - 1) / page_size

  let touch_if_non_existant fs name =
    F.create fs name >>= function
    | `Ok () -> return ()
    | `Error (`File_already_exists _) -> return ()
    | `Error err -> fail_with "error while creating file"


  let upload req body fs fid =
    let path    = "/" ^ fid in
    touch_if_non_existant fs path >>= fun () ->
    let counter     = ref 0 in
    let stream      = Cohttp_lwt_body.to_stream body in
    let file_offset = ref 0 in
    let raw_buffer  = Io_page.get 256 in
    let page_buffer = Io_page.to_cstruct raw_buffer in
    let page_buffer_offset = ref 0 in

    let buffer_to_disk () =
      let buffered_data = Cstruct.sub page_buffer 0 !page_buffer_offset in
      counter := !counter + (Cstruct.len buffered_data);
      Log.info "writting %d bytes to disk at %d" (Cstruct.len buffered_data) !counter >>= fun _ ->
      F.write fs path !file_offset buffered_data >>= fun _ ->
      let next_offset = !file_offset + (Cstruct.len buffered_data) in
      file_offset := next_offset;
      page_buffer_offset := 0;
      return ()

      (* F.write fs path !file_offset buffered_data >>= function *)
      (* | `Ok x -> *)
      (*   Log.info "written ! " >>= fun () -> *)
      (*   let next_offset = !file_offset + (Cstruct.len buffered_data) in *)
      (*   file_offset := next_offset; *)
      (*   page_buffer_offset := 0; *)
      (*   return () *)
      (* | `Error exp -> *)
      (*   match exp with *)
      (*   | `No_space -> Log.warn "No disk space" *)
      (*   | `Format_not_recognised x -> Log.warn "error non recogonised format" *)
      (*   | `Unknown_error x -> Log.warn "unkown error %s" x *)
      (*   | `Block_device x -> Log.warn "block device error" *)
      (*   | _ -> Log.warn "error occured while writing to disk" *)
      (*     >>= fun _ -> *)
      (*     F.destroy fs path >>= fun _ -> *)
      (*     return () *)
    in

    let rec batch_data data offset =
      (* Log.info "batching data at offset %d " offset >>= fun () -> *)
      let data_remaining = String.length data - offset in
      if data_remaining = 0 then return ()
      else (
        let page_buffer_free = Cstruct.len page_buffer - !page_buffer_offset in
        let chunk_size = min page_buffer_free data_remaining in
        (* Log.info "copying %d bytes buffer at offset %d" chunk_size !page_buffer_offset >>= fun () -> *)
        Cstruct.blit_from_string data offset page_buffer !page_buffer_offset chunk_size;
        page_buffer_offset := !page_buffer_offset + chunk_size;
        lwt () = if page_buffer_free = chunk_size then buffer_to_disk () else return () in
        batch_data data (offset + chunk_size)
      )
    in

    let stream_data stream =
      stream |> Lwt_stream.iter_s (fun data -> batch_data data 0) >>=
      buffer_to_disk >>= fun _ -> Log.info "stream end"
    in
    MProf.Trace.should_resolve (
    Lwt.try_bind
      (fun () -> stream_data stream)
      (fun _ -> send_reponse 200 "upload successfull")
      (fun exn ->
         Log.expception exn >>= fun () ->
         send_reponse 500 "upload failed")

);
send_reponse 200 "upload successfull"

end
