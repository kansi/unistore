open Lwt
open Cohttp

let write = ref (fun x -> print_endline x; Lwt.return ())

let info fmt = Printf.ksprintf !write ("info: " ^^ fmt)
let warn fmt = Printf.ksprintf !write ("warn: " ^^ fmt)


let print_req headers body =
  let uri = headers |> Request.uri |> Uri.to_string in
  let meth = headers |> Request.meth |> Code.string_of_method in
  body |> Cohttp_lwt_body.to_string >|= fun body ->
  (warn "received request: %s\n%s\n%s" uri meth body)


let expception exn =
  warn "error handling HTTP req: %s\n%s" (Printexc.to_string exn) (Printexc.get_backtrace ())
