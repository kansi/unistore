open Lwt
open Cohttp


module Main (C: V1_LWT.CONSOLE) (Server: Cohttp_lwt.Server) (F: V1_LWT.FS) = struct

  module Path_Handler = Handler.Make(Server)(F)


  (* map paths to handlers *)
  let handle_path req body fs =
    let uri = Uri.path req.Cohttp.Request.uri in
    let meth = Cohttp.Request.meth req in
    match uri, meth  with
    | "/", `GET -> Path_Handler.hello req body

    | "/files/list", `GET -> Path_Handler.list_files req body fs

    | "/files/upload", `PUT ->
      let headers = req |> Request.headers |> Header.to_list in
      let print_headers (k, v) = if k = "fid" then return_true else (return_false)  in
      Lwt_list.find_s print_headers headers >>= fun (_, fid) ->
      Path_Handler.upload req body fs fid

    | undef_path, _ -> Server.respond_error ~status:`Bad_request
                      ~body:(Printf.sprintf "Bad request on %s\n" undef_path) ()

  let handle_req_exn exn =
    Log.warn "error handling HTTP req: %s\n%s"
      (Printexc.to_string exn)
      (Printexc.get_backtrace ())
    >>= fun () -> raise exn

  let start c http disk =
    Log.write := C.log_s c;

    let ls fs =
      F.listdir fs "/" >>= function
      | `Ok s -> Log.info "contents:\n %s" (String.concat " " s)
      | `Error e -> Log.warn "error while listing contents\n"
    in
    ls disk >>= fun _ -> () ;

    let callback conn_id req body =
      Lwt.catch (fun () -> handle_path req body disk) handle_req_exn
    in
    let conn_closed _connn_id = Log.info "connection close" |> ignore in

    http (`TCP 9191) (Server.make ~callback ~conn_closed ())

end
